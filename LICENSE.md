Molecular symmetry chart by Luuk Kempen

To the extent possible under law, the person who associated CC0 with the
molecular symmetry chart has waived all copyright and related or neighboring
rights to the molecular symmetry chart.

You should have received a copy of the CC0 legalcode along with this
work. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
