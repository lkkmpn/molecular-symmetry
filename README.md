# Molecular symmetry chart

A chart for determining the point group of a molecule created using Ti*k*Z.

![Molecular symmetry chart](chart.svg)

## Usage

The Ti*k*Z commands for the chart are included in
`molecular-symmetry-chart.tikz`. This figure can be included in any LaTeX
document using `\input{molecular-symmetry-chart.tikz}`. The figure is created
so that it fits inside the width of an A4 paper with 1 inch margins. An example
of how to include the figure is `standalone.tex`, which includes it in a
[standalone](https://ctan.org/pkg/standalone) environment so that the figure is
the only thing in the output file.

## Different formats

To create the chart in different formats, the commands below can be used. Some
paths might be cumbersome, but these seem the best to keep font compatibility.
(Specifically, `pdf2svg` was the only tool I tested which successfully embedded
LaTeX fonts.)

The `batch-convert.sh` script automatically builds the `standalone.tex` file
and creates SVG, EPS, and PNG files. This script requires Inkscape ≥1.0, see
below for details on the different Inkscape commands.

### PDF

Use your favourite LaTeX to PDF program. Examples are given for `latexmk` and
`pdflatex`.

`latexmk -pdf -jobname=chart standalone.tex`

`pdflatex -jobname=chart standalone.tex`

### SVG

You can convert the PDF to SVG using
[pdf2svg](https://github.com/dawbarton/pdf2svg):

`pdf2svg chart.pdf chart.svg`

### EPS, PNG, etc.

From SVG, you can convert to many different formats using
[Inkscape](https://inkscape.org), such as EPS and PNG. Note that the Inkscape
command-line arguments changed with the 1.0 release.

*Inkscape 0.92.4*:

`inkscape --export-eps=chart.eps chart.svg`

`inkscape --export-png=chart.png --export-dpi=150 chart.svg`

*Inkscape ≥1.0*:

`inkscape --export-type="eps" chart.svg`

`inkscape --export-type="png" --export-dpi=150 chart.svg`

# License

This project is licensed under [the CC0 license](LICENSE.md).
