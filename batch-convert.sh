#!/bin/bash

pdflatex -jobname=chart standalone.tex
pdf2svg chart.pdf chart.svg
inkscape --export-type="eps" chart.svg
inkscape --export-type="png" --export-dpi=150 chart.svg
